#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <hdf5.h>
#include "common.h"
#include "dataxform.h"
#include "hdf5io.h"

ARRAY_BASE_TYPE *array_remove_single_hits(ARRAY_BASE_TYPE *ary)
/* ary is modified */
{
    size_t x, y, count;
    ssize_t i, j;
    
    for(y=1; y<SENSOR_NROW-1; y++) {
        for(x=1; x<SENSOR_NCOL-1; x++) {
            if(ary[SENSOR_NCOL * y + x]) {
                count = 0;
                for(j=-1; j<=1; j++)
                    for(i=-1; i<=1; i++) {
                        if(ary[SENSOR_NCOL * (y+j) + (x+i)])
                            count++;
                    }
                if(count <= 1) {
                    ary[SENSOR_NCOL * y + x] = 0;
                }
            }
        }
    }

    return ary;
}

int main(int argc, char **argv)
{
    size_t iSensor, outAryIdx, iFrame, nFrames, nFrames1;
    struct hdf5io_file *fHdl;
    BITFIELD_BASE_TYPE *bf;
    ARRAY_BASE_TYPE *ary, *fAry;

    if(argc<3) {
        error_printf("Usage: %s data.h5 iSensor(0..9) outAryIdx[current nAry]\n", argv[0]);
        return EXIT_FAILURE;
    }
    iSensor = atol(argv[2]);

    fHdl = hdf5io_open_file_for_modify(argv[1]);
    outAryIdx = hdf5io_get_array_n_frames(fHdl, iSensor);
    printf("There are %zd array frames in sensor %zd\n", outAryIdx, iSensor);

    if(argc >= 4) {
        outAryIdx = atol(argv[3]);
    }

    printf("Cleaned up sum will be written to array index %zd\n", outAryIdx);
    
    nFrames = hdf5io_get_bitfield_n_frames(fHdl, iSensor);
    printf("There are %zd bitfield frames.\n", nFrames);

    bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS);
    ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    bzero(ary, sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    fAry = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);

    for(iFrame = 0; iFrame < nFrames; iFrame++) {
        nFrames1 = 1;
        bf = hdf5io_read_bitfield_frames(fHdl, bf, iSensor, iFrame, &nFrames1);
        bitfield2array(bf, fAry);
        array_remove_single_hits(fAry);
        array_add_array(ary, fAry);
    }

    hdf5io_write_array_frames(fHdl, ary, iSensor, outAryIdx, 1, 1);
    hdf5io_close_file(fHdl);
    free(bf);
    free(ary);
    free(fAry);

    return EXIT_SUCCESS;
}
