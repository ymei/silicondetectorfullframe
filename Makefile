CC=gcc
CFLAGS=-Wall -O2
INCLUDE=-I/opt/local/include
LIBS=-L/opt/local/lib -lhdf5

.PHONY: all clean
all: raw2h5 printBitfieldFrames printArrayColSum printArrayFrames removeSingleHits
removeSingleHits: removeSingleHits.c dataxform.o hdf5io.o
	$(CC) $(CFLAGS) $(INCLUDE) $^ $(LIBS) $(LDFLAGS) -o $@
printArrayColSum: printArrayColSum.c dataxform.o hdf5io.o
	$(CC) $(CFLAGS) $(INCLUDE) $^ $(LIBS) $(LDFLAGS) -o $@
printArrayFrames: printArrayFrames.c dataxform.o hdf5io.o
	$(CC) $(CFLAGS) $(INCLUDE) $^ $(LIBS) $(LDFLAGS) -o $@
printBitfieldFrames: printBitfieldFrames.c dataxform.o hdf5io.o
	$(CC) $(CFLAGS) $(INCLUDE) $^ $(LIBS) $(LDFLAGS) -o $@
raw2h5: raw2h5.c dataxform.o hdf5io.o
	$(CC) $(CFLAGS) $(INCLUDE) $^ $(LIBS) $(LDFLAGS) -o $@
dataxform.o: dataxform.c dataxform.h common.h
	$(CC) $(CFLAGS) $(INCLUDE) -c $<
dataxform: dataxform.c dataxform.h common.h
	$(CC) $(CFLAGS) $(INCLUDE) -DDATAXFORM_DEBUG_ENABLEMAIN $< $(LIBS) $(LDFLAGS) -o $@
hdf5io.o: hdf5io.c hdf5io.h common.h
	$(CC) $(CFLAGS) -DH5_NO_DEPRECATED_SYMBOLS $(INCLUDE) -c $<
hdf5io: hdf5io.c hdf5io.h common.h
	$(CC) $(CFLAGS) -DH5_NO_DEPRECATED_SYMBOLS $(INCLUDE) -DHDF5IO_DEBUG_ENABLEMAIN $< $(LIBS) $(LDFLAGS) -o $@
clean:
	rm -f *.o
