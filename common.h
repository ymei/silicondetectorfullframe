#ifndef __COMMON_H__
#define __COMMON_H__

#define BITFIELD_BASE_TYPE uint32_t
#define BITFIELD_HDF5_TYPE H5T_NATIVE_UINT32

/* at 640us / frame, these types can hold about 15 days live time data summed */
#define ARRAY_BASE_TYPE int32_t
#define ARRAY_HDF5_TYPE H5T_NATIVE_INT32

/* sensor properties */
#define SENSOR_NCOL 960
#define SENSOR_NROW 928

#define BITFIELD_NCOL (SENSOR_NCOL / bitsof(BITFIELD_BASE_TYPE))
#define BITFIELD_NELEMENTS (SENSOR_NROW * BITFIELD_NCOL)

/* utilities */
#define bitsof(x) (8*sizeof(x))

#ifdef DEBUG
  #define debug_printf(fmt, ...) do { fprintf(stderr, fmt, ##__VA_ARGS__); fflush(stderr); \
                                    } while (0)
#else
  #define debug_printf(...) ((void)0)
#endif
#define error_printf(fmt, ...) do { fprintf(stderr, fmt, ##__VA_ARGS__); fflush(stderr); \
                                  } while(0)
#endif /* __COMMON_H__ */
