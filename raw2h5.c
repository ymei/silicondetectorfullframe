#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <hdf5.h>
#include "common.h"
#include "dataxform.h"
#include "hdf5io.h"

int main(int argc, char **argv)
{
    size_t iFile, iSensor, iFrame, nFrames;
    struct hdf5io_file *fHdl;
    BITFIELD_BASE_TYPE *bf;
    ARRAY_BASE_TYPE *ary;

    if(argc<3) {
        error_printf("Usage: %s out.h5 iSensor(0..9) file0 file1 ...\n", argv[0]);
        return EXIT_FAILURE;
    }
    iSensor = atol(argv[2]);

    nFrames = 100;
    bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS * nFrames);
    ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    bzero(ary, sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    
    fHdl = hdf5io_open_file_for_write(argv[1], 10);
    iFrame = 0;
    for(iFile = 3; iFile < argc; iFile++) {
        bf = rawfile2bitfield(argv[iFile], bf, &nFrames);
        printf("File %s contained %zd frames\n", argv[iFile], nFrames);
        hdf5io_write_bitfield_frames(fHdl, bf, iSensor, iFrame, nFrames, 0);
        iFrame += nFrames;
        array_add_bitfields(ary, bf, nFrames);
        nFrames++; /* (avoid unnecessary memory realloc in rawfile2bitfield() */
    }

    hdf5io_write_array_frames(fHdl, ary, iSensor, 0, 1, 0);
    hdf5io_close_file(fHdl);

    free(bf);
    // print_array(ary, stdout);
    free(ary);
    return EXIT_SUCCESS;
}
