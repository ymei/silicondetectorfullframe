#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <hdf5.h>
#include "common.h"
#include "dataxform.h"
#include "hdf5io.h"

int main(int argc, char **argv)
{
    size_t iSensor, iFrame, nFrames;
    size_t nTotalFrames, i;
    struct hdf5io_file *fHdl;
    BITFIELD_BASE_TYPE *bf;
    ARRAY_BASE_TYPE *ary;
    
    if(argc<5) {
        error_printf("Usage: %s data.h5 iSensor(0..9) iFrame nFrames\n", argv[0]);
        return EXIT_FAILURE;
    }
    iSensor = atol(argv[2]);
    iFrame = atol(argv[3]);
    nFrames = atol(argv[4]);
    
    fHdl = hdf5io_open_file_for_modify(argv[1]);
    nTotalFrames = hdf5io_get_bitfield_n_frames(fHdl, iSensor);
    if(iFrame >= nTotalFrames) {
        iFrame = 0;
    }
    if(iFrame + nFrames > nTotalFrames) {
        nFrames = nTotalFrames - iFrame;
    }
    error_printf("Sensor %zd has %zd total frames, printing %zd frames from index %zd\n",
                 iSensor, nTotalFrames, nFrames, iFrame);
    
    ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    bf = hdf5io_read_bitfield_frames(fHdl, NULL, iSensor, iFrame, &nFrames);
    for(i=0; i<nFrames; i++) {
        bitfield2array(bf + BITFIELD_NELEMENTS * i, ary);
        print_array(ary, stdout);
    }

    hdf5io_close_file(fHdl);
    free(bf);
    free(ary);

    return EXIT_SUCCESS;
}
