#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <hdf5.h>
#include "common.h"
#include "dataxform.h"
#include "hdf5io.h"

int main(int argc, char **argv)
{
    size_t iSensor, iFrame, nFrames;
    size_t nTotalFrames, i, x, y;
    unsigned long long sum;
    char *header;
    struct hdf5io_file *fHdl;
    ARRAY_BASE_TYPE *ary;
    
    if(argc<6) {
        error_printf("Usage: %s data.h5 iSensor(0..9) iFrame nFrames header\n", argv[0]);
        return EXIT_FAILURE;
    }
    iSensor = atol(argv[2]);
    iFrame = atol(argv[3]);
    nFrames = atol(argv[4]);
    header = argv[5];
    
    fHdl = hdf5io_open_file_for_modify(argv[1]);
    nTotalFrames = hdf5io_get_array_n_frames(fHdl, iSensor);
    if(iFrame >= nTotalFrames) {
        iFrame = 0;
    }
    if(iFrame + nFrames > nTotalFrames) {
        nFrames = nTotalFrames - iFrame;
    }
    error_printf("Sensor %zd has %zd total frames, printing %zd frames from index %zd\n",
                 iSensor, nTotalFrames, nFrames, iFrame);
    
    ary = hdf5io_read_array_frames(fHdl, NULL, iSensor, iFrame, &nFrames);
    for(i=0; i<nFrames; i++) {
        printf("%s ", header);
        for(x=0; x<SENSOR_NCOL; x++) {
            sum = 0;
            for(y=0; y<SENSOR_NROW; y++)
                sum += *(ary + (SENSOR_NCOL * SENSOR_NROW) * i + SENSOR_NCOL * y + x);
            printf("%lld ", sum);
        }
        printf("\n");
    }

    hdf5io_close_file(fHdl);
    free(ary);

    return EXIT_SUCCESS;
}
