#!/bin/bash

argc=`expr $# + 1`

usage() {
    echo "Usage: $0 rawDataDir outFileName"
}

if [ $argc -lt 3 ]; then
    usage
    exit 1 # failure
fi

rawDataDir=$1
outFileName=$2

iSensor=`basename $rawDataDir`
echo "Analyzing sensor $iSensor"

echo -n > $outFileName

for rawF in `find $rawDataDir/h5 -name '*_*.h5'`; do
    runName=`basename $rawF`

    vref2=`echo $runName|awk 'BEGIN {FS="[_.]"} {print $1}'`
    vref1=`echo $runName|awk 'BEGIN {FS="[_.]"} {print $2}'`
    echo "vref2 = $vref2, vref1 = $vref1"

    ./printArrayColSum "$rawF" $iSensor 0 1 "$vref2 $vref1" >> $outFileName
done
sort -k1,1n -k2,2n $outFileName > tmp
mv tmp $outFileName

