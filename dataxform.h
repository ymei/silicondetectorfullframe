#ifndef __DATAXFORM_H__
#define __DATAXFORM_H__

#ifdef __cplusplus
extern "C" {
#endif

BITFIELD_BASE_TYPE *array2bitfield(ARRAY_BASE_TYPE *ary, BITFIELD_BASE_TYPE *bf);
ARRAY_BASE_TYPE *bitfield2array(BITFIELD_BASE_TYPE *bf, ARRAY_BASE_TYPE *ary);
ARRAY_BASE_TYPE *array_add_array(ARRAY_BASE_TYPE *aryTo, ARRAY_BASE_TYPE *aryFrom);
ARRAY_BASE_TYPE *array_add_bitfields(ARRAY_BASE_TYPE *ary, BITFIELD_BASE_TYPE *bf, size_t nFrames);
BITFIELD_BASE_TYPE *raw2bitfield(char *raw, BITFIELD_BASE_TYPE *bf, size_t nFrames);
BITFIELD_BASE_TYPE *rawfile2bitfield(char *fname, BITFIELD_BASE_TYPE *bf, size_t *nFrames);
int print_array(ARRAY_BASE_TYPE *ary, FILE *fp);

#ifdef __cplusplus
}
#endif
#endif /* __DATAXFORM_H__ */
