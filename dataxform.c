#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <hdf5.h>
#include "common.h"
#include "dataxform.h"

BITFIELD_BASE_TYPE *array2bitfield(ARRAY_BASE_TYPE *ary, BITFIELD_BASE_TYPE *bf)
{
    size_t x, y, ix, iS;
    
    if(bf == NULL) {
        bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS);
    }
    bzero(bf, sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS);

    for(y=0; y<SENSOR_NROW; y++) {
        for(x=0; x<SENSOR_NCOL; x++) {
            ix = x / bitsof(BITFIELD_BASE_TYPE);
            iS = x % bitsof(BITFIELD_BASE_TYPE);
            bf[BITFIELD_NCOL * y + ix] |= (ary[SENSOR_NCOL * y + x] & ((ARRAY_BASE_TYPE)0x1)) << iS;
        }
    }

    return bf;
}

ARRAY_BASE_TYPE *bitfield2array(BITFIELD_BASE_TYPE *bf, ARRAY_BASE_TYPE *ary)
{
    size_t x, y, ix, iS;

    if(ary == NULL) {
        ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    }
    for(y=0; y<SENSOR_NROW; y++) {
        x = 0;
        for(ix=0; ix<BITFIELD_NCOL; ix++) {
            for(iS=0; iS<bitsof(BITFIELD_BASE_TYPE); iS++) {
                ary[SENSOR_NCOL * y + x] = (bf[BITFIELD_NCOL * y + ix] >> iS)
                                           & ((BITFIELD_BASE_TYPE)0x1);
                x++;
            }
        }
    }

    return ary;
}

ARRAY_BASE_TYPE *array_add_array(ARRAY_BASE_TYPE *aryTo, ARRAY_BASE_TYPE *aryFrom)
{
    size_t x, y;

    for(y=0; y<SENSOR_NROW; y++) {
        for(x=0; x<SENSOR_NCOL; x++) {
            aryTo[SENSOR_NCOL * y + x] += aryFrom[SENSOR_NCOL * y + x];
        }
    }
    
    return aryTo;
}

ARRAY_BASE_TYPE *array_add_bitfields(ARRAY_BASE_TYPE *ary, BITFIELD_BASE_TYPE *bf, size_t nFrames)
{
    size_t x, y, iF, ix, iS;

    if(ary == NULL) {
        ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
        bzero(ary, sizeof(ARRAY_BASE_TYPE) * SENSOR_NCOL * SENSOR_NROW);
    }

    for(iF=0; iF<nFrames; iF++) {
        for(y=0; y<SENSOR_NROW; y++) {
            x = 0;
            for(ix=0; ix<BITFIELD_NCOL; ix++) {
                for(iS=0; iS<bitsof(BITFIELD_BASE_TYPE); iS++) {
                    ary[SENSOR_NCOL * y + x] +=
                        (bf[BITFIELD_NELEMENTS * iF + BITFIELD_NCOL * y + ix] >> iS)
                        & ((BITFIELD_BASE_TYPE)0x1);
                    x++;
                }
            }
        }
    }

    return ary;
}

BITFIELD_BASE_TYPE *raw2bitfield(char *raw, BITFIELD_BASE_TYPE *bf, size_t nFrames)
{
    if(bf == NULL) {
        bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS * nFrames);
    }
    bzero(bf, sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS * nFrames);

    return bf;
}

BITFIELD_BASE_TYPE *rawfile2bitfield(char *fName, BITFIELD_BASE_TYPE *bf, size_t *nFrames)
{
    const uint16_t frameHeader = 0xffff;
    const uint16_t frameTrailer = 0xaaaa;

    size_t rawFrameLen;
    uint16_t *rawFrame = NULL;
    uint16_t dataLength0, dataLength1;
    uint32_t frameCounter;
    size_t i, x=0, y=0, iRaw, iFrame, statesLeft, nContiguous;
    size_t reqSize, retNObj;
    /* states */
    int sInFrame;
    BITFIELD_BASE_TYPE bit=1;
    
    FILE *fp;

    if((fp = fopen(fName, "rb")) == NULL) {
        perror(fName);
        return NULL;
    }
    fseek(fp, 0L, SEEK_END);
    rawFrameLen = ftell(fp) / sizeof(uint16_t);
    fseek(fp, 0L, SEEK_SET);
    debug_printf("rawFrameLen: %zd\n", rawFrameLen);

    /* *nFrames should contain a guessed number here first */
    if(bf == NULL) {
        bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS
                                         * (*nFrames));
    }
    bzero(bf, sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS * (*nFrames));

    rawFrame = (uint16_t*)malloc(rawFrameLen * sizeof(uint16_t));
    reqSize = rawFrameLen * sizeof(uint16_t);
    retNObj = fread(rawFrame, reqSize, 1, fp);

    if(retNObj < 1) {
        error_printf("Read raw frame error.\n");
        free(rawFrame);
        return NULL;
    }

    iFrame = 0;
    sInFrame = 0;
    statesLeft = 0;
    for(iRaw = 0; iRaw < rawFrameLen; iRaw++) {
        if(sInFrame) {
            if(rawFrame[iRaw] == frameTrailer) {
                iRaw++;
                if(rawFrame[iRaw] == frameTrailer) {
                    sInFrame = 0;
                    iFrame++;
                    /* make more room (double the size) in bf if necessary */
                    if(iFrame >= *nFrames) {
                        bf = realloc(bf, sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS
                                     * (*nFrames * 2));
                        bzero(bf + BITFIELD_NELEMENTS * (*nFrames),
                              sizeof(BITFIELD_BASE_TYPE) * BITFIELD_NELEMENTS * (*nFrames));
                        *nFrames *= 2;
                    }
                    debug_printf("\n\n");
                    continue;
                } else {
                    error_printf("Trailer detection error at iRaw = %zd\n", iRaw);
                    continue;
                }
            } else if(statesLeft == 0) { /* read row header */
                statesLeft = rawFrame[iRaw] & 0x000f;
                y = (rawFrame[iRaw] >> 4) & 0x03ff;
            } else { /* read a state */
                x = (rawFrame[iRaw] >> 2) & 0x03ff;
                nContiguous = (rawFrame[iRaw] & 0x0003) + 1;
                if(y >= SENSOR_NROW || x >= SENSOR_NCOL) {
                    error_printf("(x, y) = (%zd, %zd) at iRaw = %zd (0x%04x) is out of bounds.\n",
                                 x, y, iRaw, rawFrame[iRaw]);
                } else {
                    // debug_printf("%4zd %4zd %4zd\n", x, y, nContiguous);
                    for(i=0; i<nContiguous; i++)
                        debug_printf("%4zd %4zd %d\n", x+i, y, 1);
                    bf[BITFIELD_NELEMENTS * iFrame + BITFIELD_NCOL * y
                       + x/bitsof(BITFIELD_BASE_TYPE)] |= bit << (x % bitsof(BITFIELD_BASE_TYPE));
                }
                statesLeft--;
            }
        } else if(rawFrame[iRaw] == frameHeader) {
            iRaw++;
            if(rawFrame[iRaw] == frameHeader) {
                sInFrame = 1;
                /* read frame counter */
                iRaw++;
                frameCounter = *((uint32_t *)(rawFrame + iRaw));
                iRaw +=2 ;
                dataLength0 = rawFrame[iRaw];
                iRaw++;
                dataLength1 = rawFrame[iRaw];

                if(dataLength0 != dataLength1) {
                    error_printf("(dataLength0 == %d) != (dataLength1 == %d)\n",
                                 dataLength0, dataLength1);
                }
                debug_printf("# iFrame = %zd, frameCounter = 0x%x (%d),"
                             " dataLength0 = %d, dataLength1 = %d\n",
                             iFrame, frameCounter, frameCounter, dataLength0, dataLength1);
                statesLeft = 0;
                continue;
            } else {
                error_printf("Header detection error at iRaw = %zd\n", iRaw);
                continue;
            }
        }
    }

    /* If not finding frameTrailer, iFrame still points to the last frame (not incremented) */
    *nFrames = iFrame;

    free(rawFrame);
    fclose(fp);
    return bf;
}

int print_array(ARRAY_BASE_TYPE *ary, FILE *fp)
{
    size_t x, y;
    for(y=0; y<SENSOR_NROW; y++) {
        for(x=0; x<SENSOR_NCOL; x++) {
            fprintf(fp, " %d", ary[y*SENSOR_NCOL + x]);
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "\n\n");
    return 0;
}

#ifdef DATAXFORM_DEBUG_ENABLEMAIN
int main(int argc, char **argv) 
{
    size_t nFrames;
    BITFIELD_BASE_TYPE *bf=NULL;
    ARRAY_BASE_TYPE *ary=NULL;

    size_t x, y;

    bf = rawfile2bitfield(argv[1], NULL, &nFrames);
    debug_printf("nFrames: %zd\n", nFrames);

    ary = array_add_bitfields(NULL, bf, nFrames);

    for(y=0; y<SENSOR_NROW; y++) {
        for(x=0; x<SENSOR_NCOL; x++) {
            printf(" %d", ary[y*SENSOR_NCOL + x]);
        }
        printf("\n");
    }

    free(bf);
    free(ary);
    return EXIT_SUCCESS;
}
#endif
