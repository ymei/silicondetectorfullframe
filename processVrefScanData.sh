#!/bin/bash

argc=`expr $# + 1`

usage() {
    echo "Usage: $0 rawDataDir outDataDir"
}

if [ $argc -lt 3 ]; then
    usage
    exit 1 # failure
fi

rawDataDir=$1
outDataDir=$2

iSensor=`basename $rawDataDir`
echo "Processing sensor $iSensor"
if [ -e $outDataDir ]; then
    echo "Output to existing directory $outDataDir"
else
    mkdir $outDataDir
    echo "Output to newly created directory $outDataDir"
fi

for rawF in `find $rawDataDir -name 'rec0.sun'`; do
    rawD=`dirname $rawF`
    runName=`basename $rawD`

    vref2=`echo $runName|awk 'BEGIN {FS="_"} {print $1}'`
    vref1=`echo $runName|awk 'BEGIN {FS="_"} {print $2}'`
    echo "vref2 = $vref2, vref1 = $vref1"

    ./raw2h5 $outDataDir/$runName.h5 $iSensor $rawD/rec*
done
