#ifndef __HDF5IO_H__
#define __HDF5IO_H__

#ifdef __cplusplus
extern "C" {
#endif

#define HDF5IO_NAME_BUF_SIZE 256

struct hdf5io_file
{
    hid_t fId;
    size_t nSensors;
};

struct hdf5io_file *hdf5io_open_file_for_write(const char *fName, size_t nSensors);
struct hdf5io_file *hdf5io_open_file_for_read(const char *fName);
/* Assuming the file/data structure is already in place.  It does not
 * attempt to establish the file/data structure. */
struct hdf5io_file *hdf5io_open_file_for_modify(const char *fName);
int hdf5io_close_file(struct hdf5io_file *fHdl);
int hdf5io_flush_file(struct hdf5io_file *fHdl);

int hdf5io_write_bitfield_frames(struct hdf5io_file *fHdl, BITFIELD_BASE_TYPE *bf,
                                 size_t iSensor, size_t start, size_t nFrames, int shrinkQ);
/* if start < 0, read all, and return number of frames into *nFrames */
BITFIELD_BASE_TYPE *hdf5io_read_bitfield_frames(struct hdf5io_file *fHdl, BITFIELD_BASE_TYPE *bf,
                                                size_t iSensor, ssize_t start, size_t *nFrames);
size_t hdf5io_get_bitfield_n_frames(struct hdf5io_file *fHdl, size_t iSensor);
int hdf5io_write_array_frames(struct hdf5io_file *fHdl, ARRAY_BASE_TYPE *ary,
                              size_t iSensor, size_t start, size_t nFrames, int shrinkQ);
/* if start < 0, read all, and return number of frames into *nFrames */
ARRAY_BASE_TYPE *hdf5io_read_array_frames(struct hdf5io_file *fHdl, ARRAY_BASE_TYPE *ary,
                                          size_t iSensor, ssize_t start, size_t *nFrames);
size_t hdf5io_get_array_n_frames(struct hdf5io_file *fHdl, size_t iSensor);


#ifdef __cplusplus
}
#endif
#endif /* __HDF5IO_H__ */
