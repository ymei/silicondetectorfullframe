#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <hdf5.h>
#include "common.h"
#include "hdf5io.h"

struct hdf5io_file *hdf5io_open_file_for_write(const char *fName, size_t nSensors)
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    size_t iSensor;
    hid_t rootGid, sensorGid, attrSid, attrAid, dSid, dPid, dTid, dDid;
    herr_t ret;
    hsize_t dims[3], h5chunkDims[3], maxDims[3];
    struct hdf5io_file *fHdl;

    fHdl = (struct hdf5io_file *)malloc(sizeof(struct hdf5io_file));
    fHdl->nSensors = nSensors;
    fHdl->fId = H5Fcreate(fName, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    rootGid = H5Gopen(fHdl->fId, "/", H5P_DEFAULT);
    {
        attrSid = H5Screate(H5S_SCALAR);
        attrAid = H5Acreate(rootGid, "nSensors", H5T_NATIVE_HSIZE, attrSid,
                            H5P_DEFAULT, H5P_DEFAULT);
        ret = H5Awrite(attrAid, H5T_NATIVE_HSIZE, &(fHdl->nSensors));
        H5Sclose(attrSid);
        H5Aclose(attrAid);
    }
    /* Create directories and datasets corresponding to all the sensors*/
    for(iSensor=0; iSensor<fHdl->nSensors; iSensor++) {
        snprintf(buf, sizeof(buf), "/Sensor%zd", iSensor);
        sensorGid = H5Gcreate(fHdl->fId, buf, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

        /* BitfieldFrames */
        snprintf(buf, sizeof(buf), "BitfieldFrames");
        dims[0] = 0;
        dims[1] = SENSOR_NROW;
        dims[2] = BITFIELD_NCOL;
        h5chunkDims[0] = 1;
        h5chunkDims[1] = SENSOR_NROW;
        h5chunkDims[2] = BITFIELD_NCOL;
        maxDims[0] = H5S_UNLIMITED;
        maxDims[1] = SENSOR_NROW;
        maxDims[2] = BITFIELD_NCOL;

        dSid = H5Screate_simple(3, dims, maxDims);
        dPid = H5Pcreate(H5P_DATASET_CREATE);
        H5Pset_chunk(dPid, 3, h5chunkDims);
        H5Pset_deflate(dPid, 6);
        
        dTid = H5Tcopy(BITFIELD_HDF5_TYPE);
        dDid = H5Dcreate(sensorGid, buf, dTid, dSid, H5P_DEFAULT, dPid, H5P_DEFAULT);

        H5Dclose(dDid);
        H5Tclose(dTid);
        H5Pclose(dPid);
        H5Sclose(dSid);

        /* ArrayFrames */
        snprintf(buf, sizeof(buf), "ArrayFrames");
        dims[0] = 0;
        dims[1] = SENSOR_NROW;
        dims[2] = SENSOR_NCOL;
        h5chunkDims[0] = 1;
        h5chunkDims[1] = SENSOR_NROW;
        h5chunkDims[2] = SENSOR_NCOL;
        maxDims[0] = H5S_UNLIMITED;
        maxDims[1] = SENSOR_NROW;
        maxDims[2] = SENSOR_NCOL;

        dSid = H5Screate_simple(3, dims, maxDims);
        dPid = H5Pcreate(H5P_DATASET_CREATE);
        H5Pset_chunk(dPid, 3, h5chunkDims);
        H5Pset_deflate(dPid, 6);
        
        dTid = H5Tcopy(ARRAY_HDF5_TYPE);
        dDid = H5Dcreate(sensorGid, buf, dTid, dSid, H5P_DEFAULT, dPid, H5P_DEFAULT);

        H5Dclose(dDid);
        H5Tclose(dTid);
        H5Pclose(dPid);
        H5Sclose(dSid);
        
        H5Gclose(sensorGid);
    }

    H5Gclose(rootGid);

    return fHdl;
}

struct hdf5io_file *hdf5io_open_file_for_read(const char *fName)
{
    hid_t attrAid;
    herr_t ret;
    struct hdf5io_file *fHdl;

    fHdl = (struct hdf5io_file *)malloc(sizeof(struct hdf5io_file));
    fHdl->fId = H5Fopen(fName, H5F_ACC_RDONLY, H5P_DEFAULT);
    {
        attrAid = H5Aopen_by_name(fHdl->fId, "/", "nSensors",
                                  H5P_DEFAULT, H5P_DEFAULT);
        ret = H5Aread(attrAid, H5T_NATIVE_HSIZE, &(fHdl->nSensors));
        H5Aclose(attrAid);
    }
    return fHdl;
}

struct hdf5io_file *hdf5io_open_file_for_modify(const char *fName)
/* Assuming the file/data structure is already in place.  It does not
 * attempt to establish the file/data structure. */
{
    hid_t attrAid;
    herr_t ret;
    struct hdf5io_file *fHdl;

    fHdl = (struct hdf5io_file *)malloc(sizeof(struct hdf5io_file));
    fHdl->fId = H5Fopen(fName, H5F_ACC_RDWR, H5P_DEFAULT);
    {
        attrAid = H5Aopen_by_name(fHdl->fId, "/", "nSensors",
                                  H5P_DEFAULT, H5P_DEFAULT);
        ret = H5Aread(attrAid, H5T_NATIVE_HSIZE, &(fHdl->nSensors));
        H5Aclose(attrAid);
    }
    return fHdl;
}

int hdf5io_close_file(struct hdf5io_file *fHdl)
{
    herr_t ret;

    ret = H5Fclose(fHdl->fId);
    free(fHdl);
    return (int)ret;
}

int hdf5io_flush_file(struct hdf5io_file *fHdl)
{
    herr_t ret;
    
    ret = H5Fflush(fHdl->fId, H5F_SCOPE_GLOBAL);
    return (int)ret;
}

int hdf5io_write_bitfield_frames(struct hdf5io_file *fHdl, BITFIELD_BASE_TYPE *bf,
                                 size_t iSensor, size_t start, size_t nFrames, int shrinkQ)
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    hid_t dSid, mSid, dDid;
    herr_t ret;
    hsize_t dims[3], maxDims[3], slabOff[3], slabDims[3], mOff[3];

    snprintf(buf, sizeof(buf), "/Sensor%zd/BitfieldFrames", iSensor);
    dDid = H5Dopen(fHdl->fId, buf, H5P_DEFAULT);
    dSid = H5Dget_space(dDid);
    H5Sget_simple_extent_dims(dSid, dims, maxDims);
    H5Sclose(dSid);
    slabOff[0] = start;
    slabOff[1] = 0;
    slabOff[2] = 0;;
    slabDims[0] = nFrames;
    slabDims[1] = dims[1];
    slabDims[2] = dims[2];

    if(shrinkQ) {
        dims[0] = start+nFrames;
    } else {
        dims[0] = (start+nFrames > dims[0])?(start+nFrames):(dims[0]);
    }
    H5Dset_extent(dDid, dims);
    dSid = H5Dget_space(dDid);
    H5Sselect_hyperslab(dSid, H5S_SELECT_SET, slabOff, NULL, slabDims, NULL);
    mSid = H5Screate_simple(3, slabDims, NULL);
    mOff[0] = 0; mOff[1] = 0; mOff[2] = 0;
    H5Sselect_hyperslab(mSid, H5S_SELECT_SET, mOff, NULL, slabDims, NULL);

    ret = H5Dwrite(dDid, BITFIELD_HDF5_TYPE, mSid, dSid, H5P_DEFAULT, bf);
    
    H5Sclose(mSid);
    H5Sclose(dSid);
    H5Dclose(dDid);

    return (int)ret;
}

BITFIELD_BASE_TYPE *hdf5io_read_bitfield_frames(struct hdf5io_file *fHdl, BITFIELD_BASE_TYPE *bf,
                                                size_t iSensor, ssize_t start, size_t *nFrames)
/* if start < 0, read all, and return number of frames into *nFrames */
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    hid_t dSid, mSid, dDid;
    herr_t ret;
    hsize_t dims[3], maxDims[3], slabOff[3], slabDims[3], mOff[3];

    snprintf(buf, sizeof(buf), "/Sensor%zd/BitfieldFrames", iSensor);
    dDid = H5Dopen(fHdl->fId, buf, H5P_DEFAULT);
    dSid = H5Dget_space(dDid);
    H5Sget_simple_extent_dims(dSid, dims, maxDims);

    if(start < 0)
        *nFrames = dims[0];
    if(bf == NULL)
    {
        if(start < 0)
            bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE)
                                             * dims[0] * dims[1] * dims[2]);
        else
            bf = (BITFIELD_BASE_TYPE*)malloc(sizeof(BITFIELD_BASE_TYPE)
                                             * (*nFrames) * dims[1] * dims[2]);
    }

    slabOff[0] = (start<0)?0:start;
    slabOff[1] = 0;
    slabOff[2] = 0;
    slabDims[0] = *nFrames;
    slabDims[1] = dims[1];
    slabDims[2] = dims[2];

    H5Sselect_hyperslab(dSid, H5S_SELECT_SET, slabOff, NULL, slabDims, NULL);
    mSid = H5Screate_simple(3, slabDims, NULL);
    mOff[0] = 0; mOff[1] = 0; mOff[2] = 0;
    H5Sselect_hyperslab(mSid, H5S_SELECT_SET, mOff, NULL, slabDims, NULL);

    ret = H5Dread(dDid, BITFIELD_HDF5_TYPE, mSid, dSid, H5P_DEFAULT, bf);

    H5Sclose(mSid);
    H5Sclose(dSid);
    H5Dclose(dDid);

    return bf;
}

size_t hdf5io_get_bitfield_n_frames(struct hdf5io_file *fHdl, size_t iSensor)
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    hid_t dSid, dDid;
    hsize_t dims[3], maxDims[3];

    snprintf(buf, sizeof(buf), "/Sensor%zd/BitfieldFrames", iSensor);
    dDid = H5Dopen(fHdl->fId, buf, H5P_DEFAULT);
    dSid = H5Dget_space(dDid);
    H5Sget_simple_extent_dims(dSid, dims, maxDims);

    H5Sclose(dSid);
    H5Dclose(dDid);
    return dims[0];
}

int hdf5io_write_array_frames(struct hdf5io_file *fHdl, ARRAY_BASE_TYPE *ary,
                              size_t iSensor, size_t start, size_t nFrames, int shrinkQ)
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    hid_t dSid, mSid, dDid;
    herr_t ret;
    hsize_t dims[3], maxDims[3], slabOff[3], slabDims[3], mOff[3];

    snprintf(buf, sizeof(buf), "/Sensor%zd/ArrayFrames", iSensor);
    dDid = H5Dopen(fHdl->fId, buf, H5P_DEFAULT);
    dSid = H5Dget_space(dDid);
    H5Sget_simple_extent_dims(dSid, dims, maxDims);
    H5Sclose(dSid);
    slabOff[0] = start;
    slabOff[1] = 0;
    slabOff[2] = 0;
    slabDims[0] = nFrames;
    slabDims[1] = dims[1];
    slabDims[2] = dims[2];

    if(shrinkQ) {
        dims[0] = start+nFrames;
    } else {
        dims[0] = (start+nFrames > dims[0])?(start+nFrames):(dims[0]);
    }
    H5Dset_extent(dDid, dims);
    dSid = H5Dget_space(dDid);
    H5Sselect_hyperslab(dSid, H5S_SELECT_SET, slabOff, NULL, slabDims, NULL);
    mSid = H5Screate_simple(3, slabDims, NULL);
    mOff[0] = 0; mOff[1] = 0; mOff[2] = 0;
    H5Sselect_hyperslab(mSid, H5S_SELECT_SET, mOff, NULL, slabDims, NULL);

    ret = H5Dwrite(dDid, ARRAY_HDF5_TYPE, mSid, dSid, H5P_DEFAULT, ary);
    
    H5Sclose(mSid);
    H5Sclose(dSid);
    H5Dclose(dDid);

    return (int)ret;
}

ARRAY_BASE_TYPE *hdf5io_read_array_frames(struct hdf5io_file *fHdl, ARRAY_BASE_TYPE *ary,
                                          size_t iSensor, ssize_t start, size_t *nFrames)
/* if start < 0, read all, and return number of frames into *nFrames */
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    hid_t dSid, mSid, dDid;
    herr_t ret;
    hsize_t dims[3], maxDims[3], slabOff[3], slabDims[3], mOff[3];

    snprintf(buf, sizeof(buf), "/Sensor%zd/ArrayFrames", iSensor);
    dDid = H5Dopen(fHdl->fId, buf, H5P_DEFAULT);
    dSid = H5Dget_space(dDid);
    H5Sget_simple_extent_dims(dSid, dims, maxDims);

    if(start < 0)
        *nFrames = dims[0];
    if(ary == NULL)
    {
        if(start < 0)
            ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * dims[0] * dims[1] * dims[2]);
        else
            ary = (ARRAY_BASE_TYPE*)malloc(sizeof(ARRAY_BASE_TYPE) * (*nFrames) * dims[1]*dims[2]);
    }

    slabOff[0] = (start<0)?0:start;
    slabOff[1] = 0;
    slabOff[2] = 0;
    slabDims[0] = *nFrames;
    slabDims[1] = dims[1];
    slabDims[2] = dims[2];

    H5Sselect_hyperslab(dSid, H5S_SELECT_SET, slabOff, NULL, slabDims, NULL);
    mSid = H5Screate_simple(3, slabDims, NULL);
    mOff[0] = 0; mOff[1] = 0; mOff[2] = 0;
    H5Sselect_hyperslab(mSid, H5S_SELECT_SET, mOff, NULL, slabDims, NULL);

    ret = H5Dread(dDid, ARRAY_HDF5_TYPE, mSid, dSid, H5P_DEFAULT, ary);

    H5Sclose(mSid);
    H5Sclose(dSid);
    H5Dclose(dDid);
    
    return ary;
}

size_t hdf5io_get_array_n_frames(struct hdf5io_file *fHdl, size_t iSensor)
{
    char buf[HDF5IO_NAME_BUF_SIZE];
    hid_t dSid, dDid;
    hsize_t dims[3], maxDims[3];

    snprintf(buf, sizeof(buf), "/Sensor%zd/ArrayFrames", iSensor);
    dDid = H5Dopen(fHdl->fId, buf, H5P_DEFAULT);
    dSid = H5Dget_space(dDid);
    H5Sget_simple_extent_dims(dSid, dims, maxDims);

    H5Sclose(dSid);
    H5Dclose(dDid);
    return dims[0];
}

#ifdef HDF5IO_DEBUG_ENABLEMAIN
int main(int argc, char **argv)
{
    struct hdf5io_file *fHdl;
    
    fHdl = hdf5io_open_file_for_write("test.h5", 10);
    hdf5io_close_file(fHdl);
    
    return EXIT_SUCCESS;
}
#endif
